 // import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
// import {Fragment} from 'react' // React Fragments allows us to return multiple elements
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Routes } from 'react-router-dom';



import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CourseView from './pages/CourseView';

// s54 additional example
import Settings from './pages/Settings';


// s54
import { UserProvider } from './UserContext';
import {useState, useEffect, useContext} from 'react'


function App() {

  // 1 - create state
  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () =>{
    localStorage.clear();
  }

  return (

    // 2 - provide state
    <UserProvider value= {{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar/>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>} />
              <Route path="/courses/:courseId" element={<CourseView/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/settings" element={<Settings/>} />
            <Route path="*" element={<Error/>}/>

          </Routes>    
        </Container>
      </Router>
    </UserProvider>

    /*
      <Router>
        <Routes>
          <Route path="/yourDesiredPayh" element={ <Componen/>} />
        </Routs>
      </Router>
    */
  );
}

export default App;
