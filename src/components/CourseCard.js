import { Card, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {

  const {_id, name, description, price} = courseProp;
  

  return (
      <Card>
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>  
          </Card.Body>
      </Card>
  )
}
















// [ACTIVITY - s50]
// export default function CourseCard(){
//   return(
//     <Card>
//       <Card.Body>
//         <Card.Title>Sample Course</Card.Title>
//         <div className = "cardSpacing">
//           <Card.Text>Description:</Card.Text>
//           <Card.Text>This is a sample course offering.</Card.Text>
//         </div>

//         <div className = "cardSpacing pt-4">
//           <Card.Text>Price:</Card.Text>
//           <Card.Text>PhP 40,000</Card.Text>
//         </div>

//         <Button variant="primary" className="mt-4">Enroll</Button>
//       </Card.Body>
//     </Card>
//   )
// }

// [ACTIVITY - s51]
//function Enroll(){
    // setCount(count + 1);
    // setSeat(seats - 1);

    // if(seats <= 0 ){
    //   setSeat(seats + 0)
    //   setCount(count + 0)
    //   return alert('No more seats.');
    // }
    // else{
    //   setSeat(seats -1)
    //   setCount(count +1)
    // }
    // console.log('Enrollees: ' + count);
//}