import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2'

export default function Login() {

	// 3 - use the state
	// variable, setter function
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);


	/*
		function authenticate(e){
			// event.preventDefault();

			// Clear input fields after submission
			localStorage.setItem('email', email);

			setUser({
				email: localStorage.getItem('email')
			})

			setEmail('');
			setPassword('');
			alert('You are now logged in.');
		}
	*/

	function authenticate(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: { 'Content-type' : 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			console.log("Check accessToken");
			console.log(data.accessToken);
			
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			}

			else{
				Swal.fire({
					title:"Authentication failed",
					icon: "success",
					text: "Check your login details and try again."
				})
			}
		})

		setEmail('');
	}

	const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])



	return(
		(user.id !== null)
		? // true - means email field is not null or has a value
		<Navigate to ="/courses"/>
		: // false - means email field is not succesfully set
		
		<Form onSubmit={(e => authenticate(e))}>
		<h2>Login</h2>
		    <Form.Group controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		            type="email" 
		            placeholder="Enter email" 
		            value = {email}
		            onChange = {e => setEmail(e.target.value)}
		            required
		        />
		    </Form.Group>

		    <Form.Group controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		            type="password" 
		            placeholder="Password"
		            value = {password}
		            onChange = {p => setPassword(p.target.value)}
		            required
		        />
		    </Form.Group>

		    { isActive ?
			    <Button variant="success mt-2" type="submit" id="submitBtn">
			    	Login
			    </Button>
			    :
			    <Button variant="success mt-2" type="submit" id="submitBtn" disabled>
			    	Login
			    </Button>
		 		}

		</Form>

	)

}