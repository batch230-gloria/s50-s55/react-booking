import Banner from '../components/Banner';

export default function Error() {

	const data = {
		title: "404 - Not found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back home"
	}

	return(
		<Banner data={data}/>
	)
}





// [Activity - s53]
/*import {Fragment} from 'react';

export default function Error(){
	
	return(
		<Fragment>
			<h1>Page Not Found</h1>
			<p>Go back to the <a href="/" className="underline">homepage</a>.</p>
		</Fragment>		
	)
}*/