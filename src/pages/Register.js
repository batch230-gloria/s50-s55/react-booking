import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate, Link, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();


	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	/*
		function registerUser(event){
			// Prevents page redirection via form submission
			// event.preventDefault();

			// Clear input fields
			setFirstName('');
			setLastName('');
			setEmail('');
			setMobileNumber('');
			setPassword1('');
			setPassword2('');

	}*/

	const registerUser = () => {

	 	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
	 		method: "POST",
	 		headers: { 'Content-type' : 'application/json' },
	 		body: JSON.stringify({
	 			firstName: firstName,
	 			lastName: lastName,
	 			email: email,
	 			mobileNumber: mobileNumber,
	 			password: password1,
	 		})

		 })
 		.then(res => res.json())
 		.then(data => {

 		console.log(data);

			if(data){

				Swal.fire({
					title: "Registration successful",
					icon: "success",
					text: "Welcome to Zuitt"
				})
				navigate("/login")
			}
			else{
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email."
				})
			}
	 		
	 	})
	 }


	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNumber !== '' && password1 !== '' && password2 !== '') &&(password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},  [firstName, lastName,email, mobileNumber, password1, password2])
	



	return(
		(user.id !== null)
		? 
		<Navigate to ="/courses"/>
		:

		<Form onSubmit={(event => registerUser(event))}>

		<h3>Register</h3>
			<Form.Group controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		            type="text" 
		            placeholder="Enter first name" 
		            value = {firstName}
		            onChange = {e => setFirstName(e.target.value)}
		            required
		        />
		    </Form.Group>

			<Form.Group controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		            type="text" 
		            placeholder="Enter last name" 
		            value = {lastName}
		            onChange = {e => setLastName(e.target.value)}
		            required
		        />
		       
		    </Form.Group>

		    <Form.Group controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		            type="email" 
		            placeholder="Enter email" 
		            value = {email}
		            onChange = {e => setEmail(e.target.value)}
		            required
		        />
		        <Form.Text className="text-muted">
		            We'll never share your email with anyone else.
		        </Form.Text>
		    </Form.Group>

		    <Form.Group controlId="mobileNumber">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
		            type="number" 
		            placeholder="Enter Mobile Number" 
		            value = {mobileNumber}
		            onChange = {e => setMobileNumber(e.target.value)}
		            required
		        />
		       
		    </Form.Group>

		    <Form.Group controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		            type="password" 
		            placeholder="Password"
		            value = {password1}
		            onChange = {e => setPassword1(e.target.value)}
		            required
		        />
		    </Form.Group>

		    <Form.Group controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		            type="password" 
		            placeholder="Verify Password" 
		            value = {password2}
		            onChange = {e => setPassword2(e.target.value)}
		            required
		        />
		    </Form.Group>
		    { isActive ? // true

			    <Button variant="primary" type="submit" id="submitBtn">
			    	Submit	   
			    </Button>	

			    : // false

			    <Button variant="primary" type="submit" id="submitBtn" disabled>
			    	Submit	   
			    </Button>
		    }
		</Form>

	)


}