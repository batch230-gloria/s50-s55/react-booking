import React from 'react';

// >> Creates a Context Object
// A context object with object data type that can be used to store information that can be share to other components within the application
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the conext object and supply the necessary information needed in the context object
// The provider is used to create a context that can be consumed or used by other components.
export const UserProvider = UserContext.Provider;

export default UserContext;